# Drupal, GitPod & Lando

Spin up a composer based Drupal installation in your browser. (Probably under two minutes).

1. Install GitPod extension for your browser (either Google Chrome or Firefox).
2. Just click the GitPod button that appears on the right corner somewhere on the repository main page.
3. Follow the instructions if prompted.



<a href="https://gitpod.io/#https://gitlab.com/binbiriz/drupal-gitpod-lando" rel="nofollow noopener noreferrer" target="_blank" class="after:hidden"><img src="https://gitpod.io/button/open-in-gitpod.svg" alt="Open in Gitpod"></a>

## Demo

You can refer to [this video](https://www.facebook.com/kburakozdemir/videos/1128533414634374/) or [this YouTube screencast](https://www.youtube.com/watch?v=GCWZ8GN28ds).

## Troubleshooting

### composer allow-plugins

When/if prompted during installation for `Do you trust "composer/installers" to execute code and wish to enable it now? (writes "allow-plugins" to composer.json) [y,n,d,?]`, press `y` and enter.

![composer allow-plugins](images/5d660d6abe62e0db3e2cbfabe103b30f4aa39c844c01eb0106607d88cd26f3ae.png)

Note: This is resolved. Please see [Composer v2.2 prompts to authorize plugins](https://www.drupal.org/project/drupal/issues/3255749) and [Composer 2.2+ Authorized Plugins](https://www.drupal.org/node/3294646).

### Connection Problems and Termination of Installation

Sometime there may be problem while connection to `https://packages.drupal.org/8/`.

![Connection Problems](images/a9de93e6fd85ed0cfd6ae05b57897ee5c0e2a603d1bcb7dff131880b09d6dfb0.png)

If connection problem happens, installation cannot install drush and then it becomes inpossible to install Drupal via drush and installation terminates.

![Termination of Installation](images/b4a9eed23386086c331f53737e91d9189ab0e582166d417709e8a7b5464d42cb.png)

**Solution 1**:

You can try once again and begin the installation from scratch.

**Solution 2**:

First require drush again:

```bash
lando composer require drush/drush
```

![require drush](images/15ec1c2bc0efadf039b754d1556115fcaba847557e3706618dddf597a2bce151.png)

Then to install Drupal via drush, run the following command:

```bash
lando drush si -y \
  --account-pass=admin \
  --site-name='Drupal, GitPod & Lando' \
  --db-url=mysql://drupal9:drupal9@database/drupal9 \
  standard
```

![Install Drupal](images/1c6fc7750b75c3d1775f40cfd1d19825ee30c967b7de96ad27e53e3fa4fc3f11.png)

Lastly open installed Drupal site by running the following command:

```bash
gp preview $(gp url $(lando info --format=json | jq -r ".[0].urls[0]" | sed -e 's#http://localhost:\(\)#\1#'))
```

![Installed Drupal](images/eefdc1e5d942e6b33a62fac6671a6d65981511574abcfb9fc8f77f740e0daa24.png)

Admin user (/user/1) name is `admin` and password is `admin`.

## Credits

Inspired by [lando/drupal-dev-environment repository](https://github.com/lando/drupal-dev-environment).
